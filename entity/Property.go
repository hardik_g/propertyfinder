package entity

//Property entity
type Property struct {
	PropertyID        int     `json:"porperty_id"`
	Latitude          float64 `json:"latitude"`
	Longitude         float64 `json:"longitude"`
	Price             int     `json:"price"`
	NumberOfBedrooms  int     `json:"number_of_bedrooms"`
	NumberOfBathrooms int     `json:"number_of_bathrooms"`
}

type Properties []Property

type PropertyPersistence interface {
	FindProperties(UserInput) Properties
	//SaveProperty
}
