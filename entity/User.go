package entity

//UserInput entity
type UserInput struct {
	RequestID    int     `json:"id"`
	Latitude     string  `json:"latitude"`
	Longitude    string  `json:"longitude"`
	Distance     float64 `json:"distance"`
	MinBudget    int     `json:"min_budget"`
	MaxBudget    int     `json:"max_budget"`
	MinBedrooms  int     `json:"min_bedrooms"`
	MaxBedrooms  int     `json:"max_bedrooms"`
	MinBathrooms int     `json:"min_bathrooms"`
	MaxBathrooms int     `json:"max_bathrooms"`
}

type UserInputPersistence interface {
	ReadUserSearchQuery() (UserInput, error)
	// SaveSearchQuery
	// SaveSearchResult
}
