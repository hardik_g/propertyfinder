package initialize

import (
	"fmt"

	"github.com/propertyfinder/input"
	"github.com/propertyfinder/property"
)

func Load() {

	for {
		runPropertyFinder()
	}
}

func runPropertyFinder() {
	userInputPersistance := input.UserInputPersistenceInstance
	input, err := userInputPersistance.ReadUserSearchQuery()
	if err != nil {
		fmt.Println(err)
		return
	}

	propertyPersistance := property.PropertyPersistanceInstance
	propeties := propertyPersistance.FindProperties(input)
	fmt.Println(propeties)
}
