package input

import (
	"fmt"

	"github.com/propertyfinder/entity"
)

type UserInputRepo struct{}

var UserInputPersistenceInstance entity.UserInputPersistence = UserInputRepo{}

// ReadUserSearchQuery gets the input for coordinate
func (UserInputRepo) ReadUserSearchQuery() (entity.UserInput, error) {

	fmt.Println("Enter RequestID latititude longitude radius - space seperated")
	return readAndValidateInput()

}

func readAndValidateInput() (input entity.UserInput, err error) {
	_, err = fmt.Scanf("%d %s %s %f", &input.RequestID, &input.Latitude, &input.Longitude, &input.Distance)
	if err != nil {
		fmt.Println("Invalid input")
	}
	return
	// err = validateCoordinates(lat, long, radius)

}

//1 19.084709 72.874217 3.40

// //validateCoordinates validates the coordinate and radius
// func validateCoordinates(lat float64, long float64, radius float64) error {
// 	if lat <= 0 || long <= 0 || radius <= 0 {
// 		return errors.New("latitude,longitute, radius cant be less than 0")
// 	}
// 	return nil
// }
