package input

import "testing"

func TestValidateCoordinates(t *testing.T) {
	type args struct {
		lat    float64
		long   float64
		radius float64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{

		{"success", args{12.9611159, 77.636222, 100.0}, false},
		{"faliure - latitute cant be negative", args{-12.9611159, 77.636222, 100.0}, true},
		{"faliure - longitute cant be negative", args{12.9611159, -77.636222, 100.0}, true},
		{"faliure - radius cant be negative", args{-12.9611159, 77.636222, -100.0}, true},
	}
	for _, tt := range tests {
		if err := validateCoordinates(tt.args.lat, tt.args.long, tt.args.radius); (err != nil) != tt.wantErr {
			t.Errorf("%q. ValidateCoordinates() error = %v, wantErr %v", tt.name, err, tt.wantErr)
		}
	}
}
