package input

import (
	"io/ioutil"
	"log"
	"os"
)

//Readfile read file and return
func ReadFile() []byte {
	workingDir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	content, err := ioutil.ReadFile(workingDir + "/" + "properties.json")
	if err != nil {
		log.Fatal(err)
	}
	return content
}
