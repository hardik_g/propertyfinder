package main

import (
	"github.com/propertyfinder/initialize"
)

func main() {
	//Load the application
	initialize.Load()
}
