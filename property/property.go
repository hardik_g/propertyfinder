package property

import (
	"fmt"
	"math"
	"strconv"

	"github.com/propertyfinder/entity"
)

var (
	properties = entity.Properties{
		{Latitude: 0.33309100883, Longitude: 1.2728423039}, //19.084709,72.928492,3.54 Miles
		{Latitude: 0.33309100883, Longitude: 1.2728423039}, //19.084709,72.928492,3.54 Miles
	}
)

const (
	cEarthRadius = 3959 //3959 earth radius in miles
)

type PropertyRepo struct{}

var PropertyPersistanceInstance entity.PropertyPersistence = PropertyRepo{}

func (PropertyRepo) FindProperties(userInput entity.UserInput) entity.Properties {
	allproperties := getPropertyWithinBound(userInput)
	for _, property := range allproperties {
		distance := calculateDistance(userInput, property)
		fmt.Println(distance)
	}
	return properties
}

//getPropertyWithinBoundget all property in bounding rectangle
func getPropertyWithinBound(userInput entity.UserInput) entity.Properties {
	latitudeInput, _ := strconv.ParseFloat(userInput.Latitude, 64)
	longitudeInput, _ := strconv.ParseFloat(userInput.Longitude, 64)
	latInputRadian := latitudeInput * math.Pi / 180
	lonInputRadian := longitudeInput * math.Pi / 180

	deltaLat := (userInput.Distance / cEarthRadius) * math.Pi / 180
	LatLowerBound := latInputRadian - deltaLat
	LatUpperBound := latInputRadian + deltaLat

	deltaLon := math.Asin(math.Sin(userInput.Distance)/math.Cos(longitudeInput)) * math.Pi / 180

	LonLowerBound := lonInputRadian - deltaLon
	LonUpperBound := lonInputRadian + deltaLon

	//read properties from databases where lat in lat bound and long in long bound.
	//Currently reading from file for demo purpose
	//query that will be executed -
	// select * from properties where latitute>LatLowerBound and latitute<LatUpperBound
	// and longitute>LonLowerBound and longitute<LonUpperBound

	fmt.Printf("%f   %f  %f LatLowerBound  \n", LatLowerBound, LatUpperBound, deltaLat)
	fmt.Printf("%f   %f  %f LonLowerBound \n", LonLowerBound, LonUpperBound, deltaLon)

	return properties
}

//calculateDistance calculates distance betwen two coordinates
func calculateDistance(userInput entity.UserInput, property entity.Property) float64 {

	lat1, _ := strconv.ParseFloat(userInput.Latitude, 64)
	long1, _ := strconv.ParseFloat(userInput.Longitude, 64)

	phi1 := lat1 * math.Pi / 180
	lamda1 := long1 * math.Pi / 180
	phi2 := property.Latitude
	lamda2 := property.Longitude

	deltaLamda := (lamda1 - lamda2)

	deltaSigma := math.Acos(math.Sin(phi1)*math.Sin(phi2) + math.Cos(phi1)*math.Cos(phi2)*math.Cos(deltaLamda))
	fmt.Println(deltaSigma)
	return deltaSigma * cEarthRadius
}
