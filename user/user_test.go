package user

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestUser_CalculateDistance(t *testing.T) {
	type fields struct {
		UserID    int
		Name      string
		Latitude  string
		Longitude string
	}
	type args struct {
		lat1  float64
		long1 float64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    float64
		wantErr bool
	}{
		{
			"success",
			fields{Latitude: "13.2411022", UserID: 4, Name: "Ian", Longitude: "77.238335"},
			args{lat1: 12.9611159, long1: 77.636222},
			53.161402368865154,
			false,
		},
		{
			"failure - Latitude not a float",
			fields{Latitude: "xx", UserID: 4, Name: "Ian", Longitude: "77.238335"},
			args{lat1: 12.9611159, long1: 77.636222},
			0,
			true,
		},
		{
			"failure - Longitude not a float",
			fields{Latitude: "13.2411022", UserID: 4, Name: "Ian", Longitude: "xx"},
			args{lat1: 12.9611159, long1: 77.636222},
			0,
			true,
		},
	}
	for _, tt := range tests {
		user := User{
			UserID:    tt.fields.UserID,
			Name:      tt.fields.Name,
			Latitude:  tt.fields.Latitude,
			Longitude: tt.fields.Longitude,
		}
		got, err := user.CalculateDistance(tt.args.lat1, tt.args.long1)
		if (err != nil) != tt.wantErr {
			t.Errorf("%q. User.CalculateDistance() error = %v, wantErr %v", tt.name, err, tt.wantErr)
			continue
		}
		if got != tt.want {
			t.Errorf("%q. User.CalculateDistance() = %v, want %v", tt.name, got, tt.want)
		}
	}
}

func TestUsers_convertUserToMap(t *testing.T) {
	user1 := User{Latitude: "12.986375", UserID: 12, Name: "Chris", Longitude: "77.043701"}
	user2 := User{Latitude: "11.92893", UserID: 1, Name: "Alice", Longitude: "78.27699"}
	usersInput := Users{
		user1,
		user2,
	}
	want := make(UserMap)
	want[12] = user1
	want[1] = user2
	tests := []struct {
		name  string
		users Users
		want  UserMap
		want1 []int
	}{
		{
			"success",
			usersInput,
			want,
			[]int{1, 12},
		},
	}
	for _, tt := range tests {
		got, got1 := tt.users.convertUserToMap()
		if !reflect.DeepEqual(got, tt.want) {
			t.Errorf("%q. Users.convertUserToMap() got = %v, want %v", tt.name, got, tt.want)
		}
		if !reflect.DeepEqual(got1, tt.want1) {
			t.Errorf("%q. Users.convertUserToMap() got1 = %v, want %v", tt.name, got1, tt.want1)
		}
	}
}

func TestFindFriendsForUser(t *testing.T) {
	oldunMarshallUser := unMarshallUser
	defer func() {
		unMarshallUser = oldunMarshallUser
	}()
	unMarshallUser = func() (Users, error) {
		friendsjson := `[{"latitude": "14.0894797", "user_id": 8, "name": "Eoin", "longitude": "77.18671"},
	{"latitude": "13.038056", "user_id": 26, "name": "Stephen", "longitude": "76.613889"},
	{"latitude": "14.1225", "user_id": 27, "name": "Enid", "longitude": "78.143333"},
	{"latitude": "13.1229599", "user_id": 6, "name": "Theresa", "longitude": "77.2701202"},
	{"latitude": "12.2559432", "user_id": 9, "name": "Jack", "longitude": "76.1048927"},
	{"latitude": "12.240382", "user_id": 10, "name": "Georgina", "longitude": "77.972413"},
	{"latitude": "13.2411022", "user_id": 4, "name": "Ian", "longitude": "77.238335"},
	{"latitude": "13", "user_id": 13, "name": "Olive", "longitude": "76"}]`
		content := []byte(friendsjson)
		var users Users
		_ = json.Unmarshal(content, &users)
		return users, nil

	}
	type args struct {
		fromlat  float64
		fromlong float64
		radius   float64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"success",
			args{12.9611159, 77.636222, EarthRadius},
			false,
		},
	}
	for _, tt := range tests {
		if err := FindFriendsForUser(tt.args.fromlat, tt.args.fromlong, tt.args.radius); (err != nil) != tt.wantErr {
			t.Errorf("%q. FindFriendsForUser() error = %v, wantErr %v", tt.name, err, tt.wantErr)
		}
	}
}
